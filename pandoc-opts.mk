PANDOCOPTS	= --pdf-engine=lualatex \
		  --standalone \
		  --number-sections \
		  --listings \
		  -f markdown+smart \
		  -M fontsize=12pt \
		  -M papersize=a4 \
		  -M mainfont="Linux Libertine" \
		  -M monofont="Inconsolata" \
		  -M sansfont="Linux Biolinum" \
		  -M colorlinks=true \
		  -M linkcolor="black" \
		  -M urlcolor="ForestGreen" \
		  -M indent=true \
		  -V pdfa \
		  --include-in-header=../header.tex

all: press-kit.pdf

press-kit.pdf: first-page.pdf content.pdf
	gs -dNOPAUSE -sDEVICE=pdfwrite -dAutoRotatePages=/None -sOUTPUTFILE=$@ -dBATCH $+

content.pdf: content.md
	pandoc ${PANDOCOPTS} -o $@ $+

content.tex: content.md
	pandoc ${PANDOCOPTS} -o $@ $+

clean::
	rm -f content.pdf press-kit.pdf
